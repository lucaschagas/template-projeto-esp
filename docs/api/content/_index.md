# Documentação de Projeto de Circuito de Controle Automático de Bomba D’água

## 1. Introdução

A implementação do projeto pode ser vista pela foto abaixo:

![](projeto-foto.jpg)


## 2. Objetivos

Este circuito é capaz de fazer a automação em reservatórios de água controlando um dispositivo como uma bomba para encher o reservatório quando o nível estiver em um ponto estabelecido e desligar quando estiver em um segundo ponto pré-determinado. Com isso é possível efetuar a automação do sistema de água, ou seja, uma caixa d’Água automatizada, mantendo a caixa d’água sempre cheia, seja residencial, comercial ou até industrial.

## 3. Materiais utilizados

### Lista de Materiais
 * Sensores
 * Bomba d'água
 * Relés
 * ...

### Esquema Elétrico


O esquema elétrico pode ser visto por:

![](projeto-esquema.gif)


## 4. Resultados

O vídeo de demonstração pode ser visto em:
{{< https://www.youtube.com/watch?v=nA0HqbxlxnM >}}


## 5. Desafios encontrados

Ajustar os sensores de forma correta.
